/**
 * This platform was auto-generated from bxiafm-view 
 * output, please do not edit manually
 */

#include "simgrid/s4u.hpp"

using namespace simgrid;

extern "C" void load_platform()
{
    s4u::NetZone* zone = s4u::create_floyd_zone("zone6739");
    // Node pm-nod17
    s4u::Host *pmnod17cpu0 = zone->create_host("pm-nod17_CPU0", 10e9)->seal();
    s4u::Host *pmnod17cpu1 = zone->create_host("pm-nod17_CPU1", 10e9)->seal();
    s4u::Host *pmnod17nic = zone->create_host("pm-nod17_NIC", 1e9)->seal();

    // Node pm-nod16
    s4u::Host *pmnod16cpu0 = zone->create_host("pm-nod16_CPU0", 10e9)->seal();
    s4u::Host *pmnod16cpu1 = zone->create_host("pm-nod16_CPU1", 10e9)->seal();
    s4u::Host *pmnod16nic = zone->create_host("pm-nod16_NIC", 1e9)->seal();

    // Node pm-nod13
    s4u::Host *pmnod13cpu0 = zone->create_host("pm-nod13_CPU0", 10e9)->seal();
    s4u::Host *pmnod13cpu1 = zone->create_host("pm-nod13_CPU1", 10e9)->seal();
    s4u::Host *pmnod13nic = zone->create_host("pm-nod13_NIC", 1e9)->seal();

    // Node pm-nod23
    s4u::Host *pmnod23cpu0 = zone->create_host("pm-nod23_CPU0", 10e9)->seal();
    s4u::Host *pmnod23cpu1 = zone->create_host("pm-nod23_CPU1", 10e9)->seal();
    s4u::Host *pmnod23nic = zone->create_host("pm-nod23_NIC", 1e9)->seal();

    // Node pm-nod20
    s4u::Host *pmnod20cpu0 = zone->create_host("pm-nod20_CPU0", 10e9)->seal();
    s4u::Host *pmnod20cpu1 = zone->create_host("pm-nod20_CPU1", 10e9)->seal();
    s4u::Host *pmnod20nic = zone->create_host("pm-nod20_NIC", 1e9)->seal();

    // Node pm-nod22
    s4u::Host *pmnod22cpu0 = zone->create_host("pm-nod22_CPU0", 10e9)->seal();
    s4u::Host *pmnod22cpu1 = zone->create_host("pm-nod22_CPU1", 10e9)->seal();
    s4u::Host *pmnod22nic = zone->create_host("pm-nod22_NIC", 1e9)->seal();

    // Node pm-nod19
    s4u::Host *pmnod19cpu0 = zone->create_host("pm-nod19_CPU0", 10e9)->seal();
    s4u::Host *pmnod19cpu1 = zone->create_host("pm-nod19_CPU1", 10e9)->seal();
    s4u::Host *pmnod19nic = zone->create_host("pm-nod19_NIC", 1e9)->seal();

    // Node pm-nod25
    s4u::Host *pmnod25cpu0 = zone->create_host("pm-nod25_CPU0", 10e9)->seal();
    s4u::Host *pmnod25cpu1 = zone->create_host("pm-nod25_CPU1", 10e9)->seal();
    s4u::Host *pmnod25nic = zone->create_host("pm-nod25_NIC", 1e9)->seal();

    // Node pm-nod33.loc/1
    s4u::Host *pmnod33bullloc1cpu0 = zone->create_host("pm-nod33.loc/1_CPU0", 10e9)->seal();
    s4u::Host *pmnod33bullloc1cpu1 = zone->create_host("pm-nod33.loc/1_CPU1", 10e9)->seal();
    s4u::Host *pmnod33bullloc1nic = zone->create_host("pm-nod33.loc/1_NIC", 1e9)->seal();

    // Node pm-nod30.loc/1
    s4u::Host *pmnod30bullloc1cpu0 = zone->create_host("pm-nod30.loc/1_CPU0", 10e9)->seal();
    s4u::Host *pmnod30bullloc1cpu1 = zone->create_host("pm-nod30.loc/1_CPU1", 10e9)->seal();
    s4u::Host *pmnod30bullloc1nic = zone->create_host("pm-nod30.loc/1_NIC", 1e9)->seal();

    // Node pm-nod33.loc
    s4u::Host *pmnod33bullloccpu0 = zone->create_host("pm-nod33.loc_CPU0", 10e9)->seal();
    s4u::Host *pmnod33bullloccpu1 = zone->create_host("pm-nod33.loc_CPU1", 10e9)->seal();
    s4u::Host *pmnod33bulllocnic = zone->create_host("pm-nod33.loc_NIC", 1e9)->seal();

    // Node pm-nod30.loc
    s4u::Host *pmnod30bullloccpu0 = zone->create_host("pm-nod30.loc_CPU0", 10e9)->seal();
    s4u::Host *pmnod30bullloccpu1 = zone->create_host("pm-nod30.loc_CPU1", 10e9)->seal();
    s4u::Host *pmnod30bulllocnic = zone->create_host("pm-nod30.loc_NIC", 1e9)->seal();

    // Node pm-nod24
    s4u::Host *pmnod24bullxcpu0 = zone->create_host("pm-nod24_CPU0", 10e9)->seal();
    s4u::Host *pmnod24bullxcpu1 = zone->create_host("pm-nod24_CPU1", 10e9)->seal();
    s4u::Host *pmnod24bullxnic = zone->create_host("pm-nod24_NIC", 1e9)->seal();

    // Node pm-nod15
    s4u::Host *pmnod15cpu0 = zone->create_host("pm-nod15_CPU0", 10e9)->seal();
    s4u::Host *pmnod15cpu1 = zone->create_host("pm-nod15_CPU1", 10e9)->seal();
    s4u::Host *pmnod15nic = zone->create_host("pm-nod15_NIC", 1e9)->seal();

    // Node pm-nod11
    s4u::Host *pmnod11bullxcpu0 = zone->create_host("pm-nod11_CPU0", 10e9)->seal();
    s4u::Host *pmnod11bullxcpu1 = zone->create_host("pm-nod11_CPU1", 10e9)->seal();
    s4u::Host *pmnod11bullxnic = zone->create_host("pm-nod11_NIC", 1e9)->seal();

    // Node pm-nod08
    s4u::Host *pmnod08bullxcpu0 = zone->create_host("pm-nod08_CPU0", 10e9)->seal();
    s4u::Host *pmnod08bullxcpu1 = zone->create_host("pm-nod08_CPU1", 10e9)->seal();
    s4u::Host *pmnod08bullxnic = zone->create_host("pm-nod08_NIC", 1e9)->seal();

    // Node pm-nod10
    s4u::Host *pmnod10bullxcpu0 = zone->create_host("pm-nod10_CPU0", 10e9)->seal();
    s4u::Host *pmnod10bullxcpu1 = zone->create_host("pm-nod10_CPU1", 10e9)->seal();
    s4u::Host *pmnod10bullxnic = zone->create_host("pm-nod10_NIC", 1e9)->seal();

    // Node pm-nod41
    s4u::Host *pmnod41bullxcpu0 = zone->create_host("pm-nod41_CPU0", 10e9)->seal();
    s4u::Host *pmnod41bullxcpu1 = zone->create_host("pm-nod41_CPU1", 10e9)->seal();
    s4u::Host *pmnod41bullxnic = zone->create_host("pm-nod41_NIC", 1e9)->seal();

    // Node pm-nod38
    s4u::Host *pmnod38bullxcpu0 = zone->create_host("pm-nod38_CPU0", 10e9)->seal();
    s4u::Host *pmnod38bullxcpu1 = zone->create_host("pm-nod38_CPU1", 10e9)->seal();
    s4u::Host *pmnod38bullxnic = zone->create_host("pm-nod38_NIC", 1e9)->seal();

    // Node pm-nod40
    s4u::Host *pmnod40bullxcpu0 = zone->create_host("pm-nod40_CPU0", 10e9)->seal();
    s4u::Host *pmnod40bullxcpu1 = zone->create_host("pm-nod40_CPU1", 10e9)->seal();
    s4u::Host *pmnod40bullxnic = zone->create_host("pm-nod40_NIC", 1e9)->seal();

    // Node pm-nod37
    s4u::Host *pmnod37bullxcpu0 = zone->create_host("pm-nod37_CPU0", 10e9)->seal();
    s4u::Host *pmnod37bullxcpu1 = zone->create_host("pm-nod37_CPU1", 10e9)->seal();
    s4u::Host *pmnod37bullxnic = zone->create_host("pm-nod37_NIC", 1e9)->seal();

    // Node pm-nod47
    s4u::Host *pmnod47bullxcpu0 = zone->create_host("pm-nod47_CPU0", 10e9)->seal();
    s4u::Host *pmnod47bullxcpu1 = zone->create_host("pm-nod47_CPU1", 10e9)->seal();
    s4u::Host *pmnod47bullxnic = zone->create_host("pm-nod47_NIC", 1e9)->seal();

    // Node pm-nod44
    s4u::Host *pmnod44bullxcpu0 = zone->create_host("pm-nod44_CPU0", 10e9)->seal();
    s4u::Host *pmnod44bullxcpu1 = zone->create_host("pm-nod44_CPU1", 10e9)->seal();
    s4u::Host *pmnod44bullxnic = zone->create_host("pm-nod44_NIC", 1e9)->seal();

    // Node pm-nod46
    s4u::Host *pmnod46bullxcpu0 = zone->create_host("pm-nod46_CPU0", 10e9)->seal();
    s4u::Host *pmnod46bullxcpu1 = zone->create_host("pm-nod46_CPU1", 10e9)->seal();
    s4u::Host *pmnod46bullxnic = zone->create_host("pm-nod46_NIC", 1e9)->seal();

    // Node pm-nod43
    s4u::Host *pmnod43bullxcpu0 = zone->create_host("pm-nod43_CPU0", 10e9)->seal();
    s4u::Host *pmnod43bullxcpu1 = zone->create_host("pm-nod43_CPU1", 10e9)->seal();
    s4u::Host *pmnod43bullxnic = zone->create_host("pm-nod43_NIC", 1e9)->seal();

    // Node pm-nod53
    s4u::Host *pmnod53bullxcpu0 = zone->create_host("pm-nod53_CPU0", 10e9)->seal();
    s4u::Host *pmnod53bullxcpu1 = zone->create_host("pm-nod53_CPU1", 10e9)->seal();
    s4u::Host *pmnod53bullxnic = zone->create_host("pm-nod53_NIC", 1e9)->seal();

    // Node pm-nod50
    s4u::Host *pmnod50bullxcpu0 = zone->create_host("pm-nod50_CPU0", 10e9)->seal();
    s4u::Host *pmnod50bullxcpu1 = zone->create_host("pm-nod50_CPU1", 10e9)->seal();
    s4u::Host *pmnod50bullxnic = zone->create_host("pm-nod50_NIC", 1e9)->seal();

    // Node pm-nod52
    s4u::Host *pmnod52bullxcpu0 = zone->create_host("pm-nod52_CPU0", 10e9)->seal();
    s4u::Host *pmnod52bullxcpu1 = zone->create_host("pm-nod52_CPU1", 10e9)->seal();
    s4u::Host *pmnod52bullxnic = zone->create_host("pm-nod52_NIC", 1e9)->seal();

    // Node pm-nod49
    s4u::Host *pmnod49bullxcpu0 = zone->create_host("pm-nod49_CPU0", 10e9)->seal();
    s4u::Host *pmnod49bullxcpu1 = zone->create_host("pm-nod49_CPU1", 10e9)->seal();
    s4u::Host *pmnod49bullxnic = zone->create_host("pm-nod49_NIC", 1e9)->seal();

    // Node pm-nod56
    s4u::Host *pmnod56bullxcpu0 = zone->create_host("pm-nod56_CPU0", 10e9)->seal();
    s4u::Host *pmnod56bullxcpu1 = zone->create_host("pm-nod56_CPU1", 10e9)->seal();
    s4u::Host *pmnod56bullxnic = zone->create_host("pm-nod56_NIC", 1e9)->seal();

    // Node pm-nod55
    s4u::Host *pmnod55bullxcpu0 = zone->create_host("pm-nod55_CPU0", 10e9)->seal();
    s4u::Host *pmnod55bullxcpu1 = zone->create_host("pm-nod55_CPU1", 10e9)->seal();
    s4u::Host *pmnod55bullxnic = zone->create_host("pm-nod55_NIC", 1e9)->seal();

    // Node pm-nod54
    s4u::Host *pmnod54bullxcpu0 = zone->create_host("pm-nod54_CPU0", 10e9)->seal();
    s4u::Host *pmnod54bullxcpu1 = zone->create_host("pm-nod54_CPU1", 10e9)->seal();
    s4u::Host *pmnod54bullxnic = zone->create_host("pm-nod54_NIC", 1e9)->seal();

    // Node pm-nod51
    s4u::Host *pmnod51bullxcpu0 = zone->create_host("pm-nod51_CPU0", 10e9)->seal();
    s4u::Host *pmnod51bullxcpu1 = zone->create_host("pm-nod51_CPU1", 10e9)->seal();
    s4u::Host *pmnod51bullxnic = zone->create_host("pm-nod51_NIC", 1e9)->seal();

    // Node pm-nod48
    s4u::Host *pmnod48bullxcpu0 = zone->create_host("pm-nod48_CPU0", 10e9)->seal();
    s4u::Host *pmnod48bullxcpu1 = zone->create_host("pm-nod48_CPU1", 10e9)->seal();
    s4u::Host *pmnod48bullxnic = zone->create_host("pm-nod48_NIC", 1e9)->seal();

    // Node pm-nod45
    s4u::Host *pmnod45bullxcpu0 = zone->create_host("pm-nod45_CPU0", 10e9)->seal();
    s4u::Host *pmnod45bullxcpu1 = zone->create_host("pm-nod45_CPU1", 10e9)->seal();
    s4u::Host *pmnod45bullxnic = zone->create_host("pm-nod45_NIC", 1e9)->seal();

    // Node pm-nod42
    s4u::Host *pmnod42bullxcpu0 = zone->create_host("pm-nod42_CPU0", 10e9)->seal();
    s4u::Host *pmnod42bullxcpu1 = zone->create_host("pm-nod42_CPU1", 10e9)->seal();
    s4u::Host *pmnod42bullxnic = zone->create_host("pm-nod42_NIC", 1e9)->seal();

    // Node pm-nod39
    s4u::Host *pmnod39bullxcpu0 = zone->create_host("pm-nod39_CPU0", 10e9)->seal();
    s4u::Host *pmnod39bullxcpu1 = zone->create_host("pm-nod39_CPU1", 10e9)->seal();
    s4u::Host *pmnod39bullxnic = zone->create_host("pm-nod39_NIC", 1e9)->seal();

    // Node pm-nod36
    s4u::Host *pmnod36bullxcpu0 = zone->create_host("pm-nod36_CPU0", 10e9)->seal();
    s4u::Host *pmnod36bullxcpu1 = zone->create_host("pm-nod36_CPU1", 10e9)->seal();
    s4u::Host *pmnod36bullxnic = zone->create_host("pm-nod36_NIC", 1e9)->seal();

    // Node pm-nod09.loc
    s4u::Host *pmnod09bullloccpu0 = zone->create_host("pm-nod09.loc_CPU0", 10e9)->seal();
    s4u::Host *pmnod09bullloccpu1 = zone->create_host("pm-nod09.loc_CPU1", 10e9)->seal();
    s4u::Host *pmnod09bulllocnic = zone->create_host("pm-nod09.loc_NIC", 1e9)->seal();

    // Node pm-nod06
    s4u::Host *pmnod06bullxcpu0 = zone->create_host("pm-nod06_CPU0", 10e9)->seal();
    s4u::Host *pmnod06bullxcpu1 = zone->create_host("pm-nod06_CPU1", 10e9)->seal();
    s4u::Host *pmnod06bullxnic = zone->create_host("pm-nod06_NIC", 1e9)->seal();

    // Node pm-nod05
    s4u::Host *pmnod05bullxcpu0 = zone->create_host("pm-nod05_CPU0", 10e9)->seal();
    s4u::Host *pmnod05bullxcpu1 = zone->create_host("pm-nod05_CPU1", 10e9)->seal();
    s4u::Host *pmnod05bullxnic = zone->create_host("pm-nod05_NIC", 1e9)->seal();

    // Node pm-nod02
    s4u::Host *pmnod02bullxcpu0 = zone->create_host("pm-nod02_CPU0", 10e9)->seal();
    s4u::Host *pmnod02bullxcpu1 = zone->create_host("pm-nod02_CPU1", 10e9)->seal();
    s4u::Host *pmnod02bullxnic = zone->create_host("pm-nod02_NIC", 1e9)->seal();

    // Node pm-nod04
    s4u::Host *pmnod04bullxcpu0 = zone->create_host("pm-nod04_CPU0", 10e9)->seal();
    s4u::Host *pmnod04bullxcpu1 = zone->create_host("pm-nod04_CPU1", 10e9)->seal();
    s4u::Host *pmnod04bullxnic = zone->create_host("pm-nod04_NIC", 1e9)->seal();

    // Node pm-nod01
    s4u::Host *pmnod01bullxcpu0 = zone->create_host("pm-nod01_CPU0", 10e9)->seal();
    s4u::Host *pmnod01bullxcpu1 = zone->create_host("pm-nod01_CPU1", 10e9)->seal();
    s4u::Host *pmnod01bullxnic = zone->create_host("pm-nod01_NIC", 1e9)->seal();

    // Node pm-nod68.loc
    s4u::Host *pmnod68bullloccpu0 = zone->create_host("pm-nod68.loc_CPU0", 10e9)->seal();
    s4u::Host *pmnod68bullloccpu1 = zone->create_host("pm-nod68.loc_CPU1", 10e9)->seal();
    s4u::Host *pmnod68bulllocnic = zone->create_host("pm-nod68.loc_NIC", 1e9)->seal();

    // Node pm-nod70.loc
    s4u::Host *pmnod70bullloccpu0 = zone->create_host("pm-nod70.loc_CPU0", 10e9)->seal();
    s4u::Host *pmnod70bullloccpu1 = zone->create_host("pm-nod70.loc_CPU1", 10e9)->seal();
    s4u::Host *pmnod70bulllocnic = zone->create_host("pm-nod70.loc_NIC", 1e9)->seal();

    // Node pm-nod67.loc
    s4u::Host *pmnod67bullloccpu0 = zone->create_host("pm-nod67.loc_CPU0", 10e9)->seal();
    s4u::Host *pmnod67bullloccpu1 = zone->create_host("pm-nod67.loc_CPU1", 10e9)->seal();
    s4u::Host *pmnod67bulllocnic = zone->create_host("pm-nod67.loc_NIC", 1e9)->seal();

    // Node pm-nod69.loc
    s4u::Host *pmnod69bullloccpu0 = zone->create_host("pm-nod69.loc_CPU0", 10e9)->seal();
    s4u::Host *pmnod69bullloccpu1 = zone->create_host("pm-nod69.loc_CPU1", 10e9)->seal();
    s4u::Host *pmnod69bulllocnic = zone->create_host("pm-nod69.loc_NIC", 1e9)->seal();

    // Node pm-nod66.loc
    s4u::Host *pmnod66bullloccpu0 = zone->create_host("pm-nod66.loc_CPU0", 10e9)->seal();
    s4u::Host *pmnod66bullloccpu1 = zone->create_host("pm-nod66.loc_CPU1", 10e9)->seal();
    s4u::Host *pmnod66bulllocnic = zone->create_host("pm-nod66.loc_NIC", 1e9)->seal();

    // Node mngt0-1
    s4u::Host *mngt01cpu0 = zone->create_host("mngt0-1_CPU0", 10e9)->seal();
    s4u::Host *mngt01cpu1 = zone->create_host("mngt0-1_CPU1", 10e9)->seal();
    s4u::Host *mngt01nic = zone->create_host("mngt0-1_NIC", 1e9)->seal();

    // Node pm-nod00
    s4u::Host *pmnod00bullxcpu0 = zone->create_host("pm-nod00_CPU0", 10e9)->seal();
    s4u::Host *pmnod00bullxcpu1 = zone->create_host("pm-nod00_CPU1", 10e9)->seal();
    s4u::Host *pmnod00bullxnic = zone->create_host("pm-nod00_NIC", 1e9)->seal();

    kernel::routing::NetPoint* pmwmc1 = zone->create_router("pm-wmc1");
    kernel::routing::NetPoint* pmwmc2 = zone->create_router("pm-wmc2");
    kernel::routing::NetPoint* pmwmc3 = zone->create_router("pm-wmc3");
    kernel::routing::NetPoint* pmwmc6 = zone->create_router("pm-wmc6");
    kernel::routing::NetPoint* pmwmc8 = zone->create_router("pm-wmc8");

    s4u::Link *l_pmnod17nic_PCI_FAT = zone
      ->create_link("l_pmnod17nic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod17nic_PCI = zone
      ->create_link("l_pmnod17nic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod16nic_PCI_FAT = zone
      ->create_link("l_pmnod16nic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod16nic_PCI = zone
      ->create_link("l_pmnod16nic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod13nic_PCI_FAT = zone
      ->create_link("l_pmnod13nic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod13nic_PCI = zone
      ->create_link("l_pmnod13nic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod23nic_PCI_FAT = zone
      ->create_link("l_pmnod23nic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod23nic_PCI = zone
      ->create_link("l_pmnod23nic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod20nic_PCI_FAT = zone
      ->create_link("l_pmnod20nic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod20nic_PCI = zone
      ->create_link("l_pmnod20nic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod22nic_PCI_FAT = zone
      ->create_link("l_pmnod22nic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod22nic_PCI = zone
      ->create_link("l_pmnod22nic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod19nic_PCI_FAT = zone
      ->create_link("l_pmnod19nic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod19nic_PCI = zone
      ->create_link("l_pmnod19nic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod25nic_PCI_FAT = zone
      ->create_link("l_pmnod25nic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod25nic_PCI = zone
      ->create_link("l_pmnod25nic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod33bullloc1nic_PCI_FAT = zone
      ->create_link("l_pmnod33bullloc1nic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod33bullloc1nic_PCI = zone
      ->create_link("l_pmnod33bullloc1nic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod30bullloc1nic_PCI_FAT = zone
      ->create_link("l_pmnod30bullloc1nic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod30bullloc1nic_PCI = zone
      ->create_link("l_pmnod30bullloc1nic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod33bulllocnic_PCI_FAT = zone
      ->create_link("l_pmnod33bulllocnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod33bulllocnic_PCI = zone
      ->create_link("l_pmnod33bulllocnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod30bulllocnic_PCI_FAT = zone
      ->create_link("l_pmnod30bulllocnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod30bulllocnic_PCI = zone
      ->create_link("l_pmnod30bulllocnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod24bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod24bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod24bullxnic_PCI = zone
      ->create_link("l_pmnod24bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod15nic_PCI_FAT = zone
      ->create_link("l_pmnod15nic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod15nic_PCI = zone
      ->create_link("l_pmnod15nic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod11bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod11bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod11bullxnic_PCI = zone
      ->create_link("l_pmnod11bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod08bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod08bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod08bullxnic_PCI = zone
      ->create_link("l_pmnod08bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod10bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod10bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod10bullxnic_PCI = zone
      ->create_link("l_pmnod10bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod41bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod41bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod41bullxnic_PCI = zone
      ->create_link("l_pmnod41bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod38bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod38bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod38bullxnic_PCI = zone
      ->create_link("l_pmnod38bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod40bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod40bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod40bullxnic_PCI = zone
      ->create_link("l_pmnod40bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod37bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod37bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod37bullxnic_PCI = zone
      ->create_link("l_pmnod37bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod47bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod47bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod47bullxnic_PCI = zone
      ->create_link("l_pmnod47bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod44bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod44bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod44bullxnic_PCI = zone
      ->create_link("l_pmnod44bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod46bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod46bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod46bullxnic_PCI = zone
      ->create_link("l_pmnod46bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod43bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod43bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod43bullxnic_PCI = zone
      ->create_link("l_pmnod43bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod53bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod53bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod53bullxnic_PCI = zone
      ->create_link("l_pmnod53bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod50bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod50bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod50bullxnic_PCI = zone
      ->create_link("l_pmnod50bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod52bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod52bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod52bullxnic_PCI = zone
      ->create_link("l_pmnod52bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod49bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod49bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod49bullxnic_PCI = zone
      ->create_link("l_pmnod49bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod56bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod56bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod56bullxnic_PCI = zone
      ->create_link("l_pmnod56bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod55bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod55bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod55bullxnic_PCI = zone
      ->create_link("l_pmnod55bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod54bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod54bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod54bullxnic_PCI = zone
      ->create_link("l_pmnod54bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod51bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod51bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod51bullxnic_PCI = zone
      ->create_link("l_pmnod51bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod48bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod48bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod48bullxnic_PCI = zone
      ->create_link("l_pmnod48bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod45bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod45bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod45bullxnic_PCI = zone
      ->create_link("l_pmnod45bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod42bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod42bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod42bullxnic_PCI = zone
      ->create_link("l_pmnod42bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod39bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod39bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod39bullxnic_PCI = zone
      ->create_link("l_pmnod39bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod36bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod36bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod36bullxnic_PCI = zone
      ->create_link("l_pmnod36bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod09bulllocnic_PCI_FAT = zone
      ->create_link("l_pmnod09bulllocnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod09bulllocnic_PCI = zone
      ->create_link("l_pmnod09bulllocnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod06bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod06bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod06bullxnic_PCI = zone
      ->create_link("l_pmnod06bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod05bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod05bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod05bullxnic_PCI = zone
      ->create_link("l_pmnod05bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod02bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod02bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod02bullxnic_PCI = zone
      ->create_link("l_pmnod02bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod04bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod04bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod04bullxnic_PCI = zone
      ->create_link("l_pmnod04bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod01bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod01bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod01bullxnic_PCI = zone
      ->create_link("l_pmnod01bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod68bulllocnic_PCI_FAT = zone
      ->create_link("l_pmnod68bulllocnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod68bulllocnic_PCI = zone
      ->create_link("l_pmnod68bulllocnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod70bulllocnic_PCI_FAT = zone
      ->create_link("l_pmnod70bulllocnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod70bulllocnic_PCI = zone
      ->create_link("l_pmnod70bulllocnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod67bulllocnic_PCI_FAT = zone
      ->create_link("l_pmnod67bulllocnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod67bulllocnic_PCI = zone
      ->create_link("l_pmnod67bulllocnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod69bulllocnic_PCI_FAT = zone
      ->create_link("l_pmnod69bulllocnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod69bulllocnic_PCI = zone
      ->create_link("l_pmnod69bulllocnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod66bulllocnic_PCI_FAT = zone
      ->create_link("l_pmnod66bulllocnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod66bulllocnic_PCI = zone
      ->create_link("l_pmnod66bulllocnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_mngt01nic_PCI_FAT = zone
      ->create_link("l_mngt01nic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_mngt01nic_PCI = zone
      ->create_link("l_mngt01nic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmnod00bullxnic_PCI_FAT = zone
      ->create_link("l_pmnod00bullxnic_PCI_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmnod00bullxnic_PCI = zone
      ->create_link("l_pmnod00bullxnic_PCI", 15.75e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SHARED)
      ->set_latency(250e-9)
      ->seal();

    s4u::Link *l_pmwmc1_pmnod17nic = zone
      ->create_link("l_pmwmc1_pmnod17nic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc1_pmnod16nic = zone
      ->create_link("l_pmwmc1_pmnod16nic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc1_pmnod13nic = zone
      ->create_link("l_pmwmc1_pmnod13nic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc1_pmnod23nic = zone
      ->create_link("l_pmwmc1_pmnod23nic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc1_pmnod20nic = zone
      ->create_link("l_pmwmc1_pmnod20nic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc1_pmnod22nic = zone
      ->create_link("l_pmwmc1_pmnod22nic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc1_pmnod19nic = zone
      ->create_link("l_pmwmc1_pmnod19nic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc1_pmnod25nic = zone
      ->create_link("l_pmwmc1_pmnod25nic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc1_pmnod33bullloc1nic = zone
      ->create_link("l_pmwmc1_pmnod33bullloc1nic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc1_pmnod30bullloc1nic = zone
      ->create_link("l_pmwmc1_pmnod30bullloc1nic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc1_pmnod33bulllocnic = zone
      ->create_link("l_pmwmc1_pmnod33bulllocnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc1_pmnod30bulllocnic = zone
      ->create_link("l_pmwmc1_pmnod30bulllocnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc1_pmwmc8_FAT = zone
      ->create_link("l_pmwmc1_pmwmc8_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc1_pmwmc8 = zone
      ->create_link("l_pmwmc1_pmwmc8", 44.4e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmwmc1_pmwmc6_FAT = zone
      ->create_link("l_pmwmc1_pmwmc6_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc1_pmwmc6 = zone
      ->create_link("l_pmwmc1_pmwmc6", 44.4e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmwmc1_pmnod24bullxnic = zone
      ->create_link("l_pmwmc1_pmnod24bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc1_pmnod15nic = zone
      ->create_link("l_pmwmc1_pmnod15nic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod11bullxnic = zone
      ->create_link("l_pmwmc2_pmnod11bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod08bullxnic = zone
      ->create_link("l_pmwmc2_pmnod08bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod10bullxnic = zone
      ->create_link("l_pmwmc2_pmnod10bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod41bullxnic = zone
      ->create_link("l_pmwmc2_pmnod41bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod38bullxnic = zone
      ->create_link("l_pmwmc2_pmnod38bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod40bullxnic = zone
      ->create_link("l_pmwmc2_pmnod40bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod37bullxnic = zone
      ->create_link("l_pmwmc2_pmnod37bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod47bullxnic = zone
      ->create_link("l_pmwmc2_pmnod47bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod44bullxnic = zone
      ->create_link("l_pmwmc2_pmnod44bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod46bullxnic = zone
      ->create_link("l_pmwmc2_pmnod46bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod43bullxnic = zone
      ->create_link("l_pmwmc2_pmnod43bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod53bullxnic = zone
      ->create_link("l_pmwmc2_pmnod53bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod50bullxnic = zone
      ->create_link("l_pmwmc2_pmnod50bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod52bullxnic = zone
      ->create_link("l_pmwmc2_pmnod52bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod49bullxnic = zone
      ->create_link("l_pmwmc2_pmnod49bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmwmc8_FAT = zone
      ->create_link("l_pmwmc2_pmwmc8_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmwmc8 = zone
      ->create_link("l_pmwmc2_pmwmc8", 44.4e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmwmc2_pmwmc6_FAT = zone
      ->create_link("l_pmwmc2_pmwmc6_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmwmc6 = zone
      ->create_link("l_pmwmc2_pmwmc6", 44.4e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod56bullxnic = zone
      ->create_link("l_pmwmc2_pmnod56bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod55bullxnic = zone
      ->create_link("l_pmwmc2_pmnod55bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod54bullxnic = zone
      ->create_link("l_pmwmc2_pmnod54bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod51bullxnic = zone
      ->create_link("l_pmwmc2_pmnod51bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod48bullxnic = zone
      ->create_link("l_pmwmc2_pmnod48bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod45bullxnic = zone
      ->create_link("l_pmwmc2_pmnod45bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod42bullxnic = zone
      ->create_link("l_pmwmc2_pmnod42bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod39bullxnic = zone
      ->create_link("l_pmwmc2_pmnod39bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod36bullxnic = zone
      ->create_link("l_pmwmc2_pmnod36bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod09bulllocnic = zone
      ->create_link("l_pmwmc2_pmnod09bulllocnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc2_pmnod06bullxnic = zone
      ->create_link("l_pmwmc2_pmnod06bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc3_pmnod05bullxnic = zone
      ->create_link("l_pmwmc3_pmnod05bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc3_pmnod02bullxnic = zone
      ->create_link("l_pmwmc3_pmnod02bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc3_pmnod04bullxnic = zone
      ->create_link("l_pmwmc3_pmnod04bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc3_pmnod01bullxnic = zone
      ->create_link("l_pmwmc3_pmnod01bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc3_pmnod68bulllocnic = zone
      ->create_link("l_pmwmc3_pmnod68bulllocnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc3_pmnod70bulllocnic = zone
      ->create_link("l_pmwmc3_pmnod70bulllocnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc3_pmnod67bulllocnic = zone
      ->create_link("l_pmwmc3_pmnod67bulllocnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc3_pmwmc8_FAT = zone
      ->create_link("l_pmwmc3_pmwmc8_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc3_pmwmc8 = zone
      ->create_link("l_pmwmc3_pmwmc8", 44.4e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmwmc3_pmwmc6_FAT = zone
      ->create_link("l_pmwmc3_pmwmc6_FAT", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc3_pmwmc6 = zone
      ->create_link("l_pmwmc3_pmwmc6", 44.4e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(0)
      ->seal();
    s4u::Link *l_pmwmc3_pmnod69bulllocnic = zone
      ->create_link("l_pmwmc3_pmnod69bulllocnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc3_pmnod66bulllocnic = zone
      ->create_link("l_pmwmc3_pmnod66bulllocnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc3_mngt01nic = zone
      ->create_link("l_pmwmc3_mngt01nic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();
    s4u::Link *l_pmwmc3_pmnod00bullxnic = zone
      ->create_link("l_pmwmc3_pmnod00bullxnic", 11.1e9)
      ->set_sharing_policy(s4u::Link::SharingPolicy::SPLITDUPLEX)
      ->set_latency(200e-9)
      ->seal();

    std::vector<s4u::Link *> v_l_pmnod17nic_PCI({l_pmnod17nic_PCI_FAT, l_pmnod17nic_PCI});
    zone->add_route(pmnod17cpu0->get_netpoint(), pmnod17nic->get_netpoint(), nullptr, nullptr, v_l_pmnod17nic_PCI, true);
    zone->add_route(pmnod17cpu1->get_netpoint(), pmnod17nic->get_netpoint(), nullptr, nullptr, v_l_pmnod17nic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod16nic_PCI({l_pmnod16nic_PCI_FAT, l_pmnod16nic_PCI});
    zone->add_route(pmnod16cpu0->get_netpoint(), pmnod16nic->get_netpoint(), nullptr, nullptr, v_l_pmnod16nic_PCI, true);
    zone->add_route(pmnod16cpu1->get_netpoint(), pmnod16nic->get_netpoint(), nullptr, nullptr, v_l_pmnod16nic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod13nic_PCI({l_pmnod13nic_PCI_FAT, l_pmnod13nic_PCI});
    zone->add_route(pmnod13cpu0->get_netpoint(), pmnod13nic->get_netpoint(), nullptr, nullptr, v_l_pmnod13nic_PCI, true);
    zone->add_route(pmnod13cpu1->get_netpoint(), pmnod13nic->get_netpoint(), nullptr, nullptr, v_l_pmnod13nic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod23nic_PCI({l_pmnod23nic_PCI_FAT, l_pmnod23nic_PCI});
    zone->add_route(pmnod23cpu0->get_netpoint(), pmnod23nic->get_netpoint(), nullptr, nullptr, v_l_pmnod23nic_PCI, true);
    zone->add_route(pmnod23cpu1->get_netpoint(), pmnod23nic->get_netpoint(), nullptr, nullptr, v_l_pmnod23nic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod20nic_PCI({l_pmnod20nic_PCI_FAT, l_pmnod20nic_PCI});
    zone->add_route(pmnod20cpu0->get_netpoint(), pmnod20nic->get_netpoint(), nullptr, nullptr, v_l_pmnod20nic_PCI, true);
    zone->add_route(pmnod20cpu1->get_netpoint(), pmnod20nic->get_netpoint(), nullptr, nullptr, v_l_pmnod20nic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod22nic_PCI({l_pmnod22nic_PCI_FAT, l_pmnod22nic_PCI});
    zone->add_route(pmnod22cpu0->get_netpoint(), pmnod22nic->get_netpoint(), nullptr, nullptr, v_l_pmnod22nic_PCI, true);
    zone->add_route(pmnod22cpu1->get_netpoint(), pmnod22nic->get_netpoint(), nullptr, nullptr, v_l_pmnod22nic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod19nic_PCI({l_pmnod19nic_PCI_FAT, l_pmnod19nic_PCI});
    zone->add_route(pmnod19cpu0->get_netpoint(), pmnod19nic->get_netpoint(), nullptr, nullptr, v_l_pmnod19nic_PCI, true);
    zone->add_route(pmnod19cpu1->get_netpoint(), pmnod19nic->get_netpoint(), nullptr, nullptr, v_l_pmnod19nic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod25nic_PCI({l_pmnod25nic_PCI_FAT, l_pmnod25nic_PCI});
    zone->add_route(pmnod25cpu0->get_netpoint(), pmnod25nic->get_netpoint(), nullptr, nullptr, v_l_pmnod25nic_PCI, true);
    zone->add_route(pmnod25cpu1->get_netpoint(), pmnod25nic->get_netpoint(), nullptr, nullptr, v_l_pmnod25nic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod33bullloc1nic_PCI({l_pmnod33bullloc1nic_PCI_FAT, l_pmnod33bullloc1nic_PCI});
    zone->add_route(pmnod33bullloc1cpu0->get_netpoint(), pmnod33bullloc1nic->get_netpoint(), nullptr, nullptr, v_l_pmnod33bullloc1nic_PCI, true);
    zone->add_route(pmnod33bullloc1cpu1->get_netpoint(), pmnod33bullloc1nic->get_netpoint(), nullptr, nullptr, v_l_pmnod33bullloc1nic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod30bullloc1nic_PCI({l_pmnod30bullloc1nic_PCI_FAT, l_pmnod30bullloc1nic_PCI});
    zone->add_route(pmnod30bullloc1cpu0->get_netpoint(), pmnod30bullloc1nic->get_netpoint(), nullptr, nullptr, v_l_pmnod30bullloc1nic_PCI, true);
    zone->add_route(pmnod30bullloc1cpu1->get_netpoint(), pmnod30bullloc1nic->get_netpoint(), nullptr, nullptr, v_l_pmnod30bullloc1nic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod33bulllocnic_PCI({l_pmnod33bulllocnic_PCI_FAT, l_pmnod33bulllocnic_PCI});
    zone->add_route(pmnod33bullloccpu0->get_netpoint(), pmnod33bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmnod33bulllocnic_PCI, true);
    zone->add_route(pmnod33bullloccpu1->get_netpoint(), pmnod33bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmnod33bulllocnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod30bulllocnic_PCI({l_pmnod30bulllocnic_PCI_FAT, l_pmnod30bulllocnic_PCI});
    zone->add_route(pmnod30bullloccpu0->get_netpoint(), pmnod30bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmnod30bulllocnic_PCI, true);
    zone->add_route(pmnod30bullloccpu1->get_netpoint(), pmnod30bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmnod30bulllocnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod24bullxnic_PCI({l_pmnod24bullxnic_PCI_FAT, l_pmnod24bullxnic_PCI});
    zone->add_route(pmnod24bullxcpu0->get_netpoint(), pmnod24bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod24bullxnic_PCI, true);
    zone->add_route(pmnod24bullxcpu1->get_netpoint(), pmnod24bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod24bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod15nic_PCI({l_pmnod15nic_PCI_FAT, l_pmnod15nic_PCI});
    zone->add_route(pmnod15cpu0->get_netpoint(), pmnod15nic->get_netpoint(), nullptr, nullptr, v_l_pmnod15nic_PCI, true);
    zone->add_route(pmnod15cpu1->get_netpoint(), pmnod15nic->get_netpoint(), nullptr, nullptr, v_l_pmnod15nic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod11bullxnic_PCI({l_pmnod11bullxnic_PCI_FAT, l_pmnod11bullxnic_PCI});
    zone->add_route(pmnod11bullxcpu0->get_netpoint(), pmnod11bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod11bullxnic_PCI, true);
    zone->add_route(pmnod11bullxcpu1->get_netpoint(), pmnod11bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod11bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod08bullxnic_PCI({l_pmnod08bullxnic_PCI_FAT, l_pmnod08bullxnic_PCI});
    zone->add_route(pmnod08bullxcpu0->get_netpoint(), pmnod08bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod08bullxnic_PCI, true);
    zone->add_route(pmnod08bullxcpu1->get_netpoint(), pmnod08bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod08bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod10bullxnic_PCI({l_pmnod10bullxnic_PCI_FAT, l_pmnod10bullxnic_PCI});
    zone->add_route(pmnod10bullxcpu0->get_netpoint(), pmnod10bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod10bullxnic_PCI, true);
    zone->add_route(pmnod10bullxcpu1->get_netpoint(), pmnod10bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod10bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod41bullxnic_PCI({l_pmnod41bullxnic_PCI_FAT, l_pmnod41bullxnic_PCI});
    zone->add_route(pmnod41bullxcpu0->get_netpoint(), pmnod41bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod41bullxnic_PCI, true);
    zone->add_route(pmnod41bullxcpu1->get_netpoint(), pmnod41bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod41bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod38bullxnic_PCI({l_pmnod38bullxnic_PCI_FAT, l_pmnod38bullxnic_PCI});
    zone->add_route(pmnod38bullxcpu0->get_netpoint(), pmnod38bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod38bullxnic_PCI, true);
    zone->add_route(pmnod38bullxcpu1->get_netpoint(), pmnod38bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod38bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod40bullxnic_PCI({l_pmnod40bullxnic_PCI_FAT, l_pmnod40bullxnic_PCI});
    zone->add_route(pmnod40bullxcpu0->get_netpoint(), pmnod40bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod40bullxnic_PCI, true);
    zone->add_route(pmnod40bullxcpu1->get_netpoint(), pmnod40bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod40bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod37bullxnic_PCI({l_pmnod37bullxnic_PCI_FAT, l_pmnod37bullxnic_PCI});
    zone->add_route(pmnod37bullxcpu0->get_netpoint(), pmnod37bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod37bullxnic_PCI, true);
    zone->add_route(pmnod37bullxcpu1->get_netpoint(), pmnod37bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod37bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod47bullxnic_PCI({l_pmnod47bullxnic_PCI_FAT, l_pmnod47bullxnic_PCI});
    zone->add_route(pmnod47bullxcpu0->get_netpoint(), pmnod47bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod47bullxnic_PCI, true);
    zone->add_route(pmnod47bullxcpu1->get_netpoint(), pmnod47bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod47bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod44bullxnic_PCI({l_pmnod44bullxnic_PCI_FAT, l_pmnod44bullxnic_PCI});
    zone->add_route(pmnod44bullxcpu0->get_netpoint(), pmnod44bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod44bullxnic_PCI, true);
    zone->add_route(pmnod44bullxcpu1->get_netpoint(), pmnod44bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod44bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod46bullxnic_PCI({l_pmnod46bullxnic_PCI_FAT, l_pmnod46bullxnic_PCI});
    zone->add_route(pmnod46bullxcpu0->get_netpoint(), pmnod46bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod46bullxnic_PCI, true);
    zone->add_route(pmnod46bullxcpu1->get_netpoint(), pmnod46bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod46bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod43bullxnic_PCI({l_pmnod43bullxnic_PCI_FAT, l_pmnod43bullxnic_PCI});
    zone->add_route(pmnod43bullxcpu0->get_netpoint(), pmnod43bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod43bullxnic_PCI, true);
    zone->add_route(pmnod43bullxcpu1->get_netpoint(), pmnod43bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod43bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod53bullxnic_PCI({l_pmnod53bullxnic_PCI_FAT, l_pmnod53bullxnic_PCI});
    zone->add_route(pmnod53bullxcpu0->get_netpoint(), pmnod53bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod53bullxnic_PCI, true);
    zone->add_route(pmnod53bullxcpu1->get_netpoint(), pmnod53bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod53bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod50bullxnic_PCI({l_pmnod50bullxnic_PCI_FAT, l_pmnod50bullxnic_PCI});
    zone->add_route(pmnod50bullxcpu0->get_netpoint(), pmnod50bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod50bullxnic_PCI, true);
    zone->add_route(pmnod50bullxcpu1->get_netpoint(), pmnod50bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod50bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod52bullxnic_PCI({l_pmnod52bullxnic_PCI_FAT, l_pmnod52bullxnic_PCI});
    zone->add_route(pmnod52bullxcpu0->get_netpoint(), pmnod52bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod52bullxnic_PCI, true);
    zone->add_route(pmnod52bullxcpu1->get_netpoint(), pmnod52bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod52bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod49bullxnic_PCI({l_pmnod49bullxnic_PCI_FAT, l_pmnod49bullxnic_PCI});
    zone->add_route(pmnod49bullxcpu0->get_netpoint(), pmnod49bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod49bullxnic_PCI, true);
    zone->add_route(pmnod49bullxcpu1->get_netpoint(), pmnod49bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod49bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod56bullxnic_PCI({l_pmnod56bullxnic_PCI_FAT, l_pmnod56bullxnic_PCI});
    zone->add_route(pmnod56bullxcpu0->get_netpoint(), pmnod56bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod56bullxnic_PCI, true);
    zone->add_route(pmnod56bullxcpu1->get_netpoint(), pmnod56bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod56bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod55bullxnic_PCI({l_pmnod55bullxnic_PCI_FAT, l_pmnod55bullxnic_PCI});
    zone->add_route(pmnod55bullxcpu0->get_netpoint(), pmnod55bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod55bullxnic_PCI, true);
    zone->add_route(pmnod55bullxcpu1->get_netpoint(), pmnod55bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod55bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod54bullxnic_PCI({l_pmnod54bullxnic_PCI_FAT, l_pmnod54bullxnic_PCI});
    zone->add_route(pmnod54bullxcpu0->get_netpoint(), pmnod54bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod54bullxnic_PCI, true);
    zone->add_route(pmnod54bullxcpu1->get_netpoint(), pmnod54bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod54bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod51bullxnic_PCI({l_pmnod51bullxnic_PCI_FAT, l_pmnod51bullxnic_PCI});
    zone->add_route(pmnod51bullxcpu0->get_netpoint(), pmnod51bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod51bullxnic_PCI, true);
    zone->add_route(pmnod51bullxcpu1->get_netpoint(), pmnod51bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod51bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod48bullxnic_PCI({l_pmnod48bullxnic_PCI_FAT, l_pmnod48bullxnic_PCI});
    zone->add_route(pmnod48bullxcpu0->get_netpoint(), pmnod48bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod48bullxnic_PCI, true);
    zone->add_route(pmnod48bullxcpu1->get_netpoint(), pmnod48bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod48bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod45bullxnic_PCI({l_pmnod45bullxnic_PCI_FAT, l_pmnod45bullxnic_PCI});
    zone->add_route(pmnod45bullxcpu0->get_netpoint(), pmnod45bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod45bullxnic_PCI, true);
    zone->add_route(pmnod45bullxcpu1->get_netpoint(), pmnod45bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod45bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod42bullxnic_PCI({l_pmnod42bullxnic_PCI_FAT, l_pmnod42bullxnic_PCI});
    zone->add_route(pmnod42bullxcpu0->get_netpoint(), pmnod42bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod42bullxnic_PCI, true);
    zone->add_route(pmnod42bullxcpu1->get_netpoint(), pmnod42bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod42bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod39bullxnic_PCI({l_pmnod39bullxnic_PCI_FAT, l_pmnod39bullxnic_PCI});
    zone->add_route(pmnod39bullxcpu0->get_netpoint(), pmnod39bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod39bullxnic_PCI, true);
    zone->add_route(pmnod39bullxcpu1->get_netpoint(), pmnod39bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod39bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod36bullxnic_PCI({l_pmnod36bullxnic_PCI_FAT, l_pmnod36bullxnic_PCI});
    zone->add_route(pmnod36bullxcpu0->get_netpoint(), pmnod36bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod36bullxnic_PCI, true);
    zone->add_route(pmnod36bullxcpu1->get_netpoint(), pmnod36bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod36bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod09bulllocnic_PCI({l_pmnod09bulllocnic_PCI_FAT, l_pmnod09bulllocnic_PCI});
    zone->add_route(pmnod09bullloccpu0->get_netpoint(), pmnod09bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmnod09bulllocnic_PCI, true);
    zone->add_route(pmnod09bullloccpu1->get_netpoint(), pmnod09bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmnod09bulllocnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod06bullxnic_PCI({l_pmnod06bullxnic_PCI_FAT, l_pmnod06bullxnic_PCI});
    zone->add_route(pmnod06bullxcpu0->get_netpoint(), pmnod06bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod06bullxnic_PCI, true);
    zone->add_route(pmnod06bullxcpu1->get_netpoint(), pmnod06bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod06bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod05bullxnic_PCI({l_pmnod05bullxnic_PCI_FAT, l_pmnod05bullxnic_PCI});
    zone->add_route(pmnod05bullxcpu0->get_netpoint(), pmnod05bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod05bullxnic_PCI, true);
    zone->add_route(pmnod05bullxcpu1->get_netpoint(), pmnod05bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod05bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod02bullxnic_PCI({l_pmnod02bullxnic_PCI_FAT, l_pmnod02bullxnic_PCI});
    zone->add_route(pmnod02bullxcpu0->get_netpoint(), pmnod02bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod02bullxnic_PCI, true);
    zone->add_route(pmnod02bullxcpu1->get_netpoint(), pmnod02bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod02bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod04bullxnic_PCI({l_pmnod04bullxnic_PCI_FAT, l_pmnod04bullxnic_PCI});
    zone->add_route(pmnod04bullxcpu0->get_netpoint(), pmnod04bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod04bullxnic_PCI, true);
    zone->add_route(pmnod04bullxcpu1->get_netpoint(), pmnod04bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod04bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod01bullxnic_PCI({l_pmnod01bullxnic_PCI_FAT, l_pmnod01bullxnic_PCI});
    zone->add_route(pmnod01bullxcpu0->get_netpoint(), pmnod01bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod01bullxnic_PCI, true);
    zone->add_route(pmnod01bullxcpu1->get_netpoint(), pmnod01bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod01bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod68bulllocnic_PCI({l_pmnod68bulllocnic_PCI_FAT, l_pmnod68bulllocnic_PCI});
    zone->add_route(pmnod68bullloccpu0->get_netpoint(), pmnod68bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmnod68bulllocnic_PCI, true);
    zone->add_route(pmnod68bullloccpu1->get_netpoint(), pmnod68bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmnod68bulllocnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod70bulllocnic_PCI({l_pmnod70bulllocnic_PCI_FAT, l_pmnod70bulllocnic_PCI});
    zone->add_route(pmnod70bullloccpu0->get_netpoint(), pmnod70bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmnod70bulllocnic_PCI, true);
    zone->add_route(pmnod70bullloccpu1->get_netpoint(), pmnod70bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmnod70bulllocnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod67bulllocnic_PCI({l_pmnod67bulllocnic_PCI_FAT, l_pmnod67bulllocnic_PCI});
    zone->add_route(pmnod67bullloccpu0->get_netpoint(), pmnod67bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmnod67bulllocnic_PCI, true);
    zone->add_route(pmnod67bullloccpu1->get_netpoint(), pmnod67bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmnod67bulllocnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod69bulllocnic_PCI({l_pmnod69bulllocnic_PCI_FAT, l_pmnod69bulllocnic_PCI});
    zone->add_route(pmnod69bullloccpu0->get_netpoint(), pmnod69bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmnod69bulllocnic_PCI, true);
    zone->add_route(pmnod69bullloccpu1->get_netpoint(), pmnod69bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmnod69bulllocnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod66bulllocnic_PCI({l_pmnod66bulllocnic_PCI_FAT, l_pmnod66bulllocnic_PCI});
    zone->add_route(pmnod66bullloccpu0->get_netpoint(), pmnod66bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmnod66bulllocnic_PCI, true);
    zone->add_route(pmnod66bullloccpu1->get_netpoint(), pmnod66bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmnod66bulllocnic_PCI, true);
    std::vector<s4u::Link *> v_l_mngt01nic_PCI({l_mngt01nic_PCI_FAT, l_mngt01nic_PCI});
    zone->add_route(mngt01cpu0->get_netpoint(), mngt01nic->get_netpoint(), nullptr, nullptr, v_l_mngt01nic_PCI, true);
    zone->add_route(mngt01cpu1->get_netpoint(), mngt01nic->get_netpoint(), nullptr, nullptr, v_l_mngt01nic_PCI, true);
    std::vector<s4u::Link *> v_l_pmnod00bullxnic_PCI({l_pmnod00bullxnic_PCI_FAT, l_pmnod00bullxnic_PCI});
    zone->add_route(pmnod00bullxcpu0->get_netpoint(), pmnod00bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod00bullxnic_PCI, true);
    zone->add_route(pmnod00bullxcpu1->get_netpoint(), pmnod00bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmnod00bullxnic_PCI, true);
    std::vector<s4u::Link *> v_l_pmwmc1_pmnod17nic({l_pmwmc1_pmnod17nic});
    zone->add_route(pmwmc1, pmnod17nic->get_netpoint(), nullptr, nullptr, v_l_pmwmc1_pmnod17nic, true);
    std::vector<s4u::Link *> v_l_pmwmc1_pmnod16nic({l_pmwmc1_pmnod16nic});
    zone->add_route(pmwmc1, pmnod16nic->get_netpoint(), nullptr, nullptr, v_l_pmwmc1_pmnod16nic, true);
    std::vector<s4u::Link *> v_l_pmwmc1_pmnod13nic({l_pmwmc1_pmnod13nic});
    zone->add_route(pmwmc1, pmnod13nic->get_netpoint(), nullptr, nullptr, v_l_pmwmc1_pmnod13nic, true);
    std::vector<s4u::Link *> v_l_pmwmc1_pmnod23nic({l_pmwmc1_pmnod23nic});
    zone->add_route(pmwmc1, pmnod23nic->get_netpoint(), nullptr, nullptr, v_l_pmwmc1_pmnod23nic, true);
    std::vector<s4u::Link *> v_l_pmwmc1_pmnod20nic({l_pmwmc1_pmnod20nic});
    zone->add_route(pmwmc1, pmnod20nic->get_netpoint(), nullptr, nullptr, v_l_pmwmc1_pmnod20nic, true);
    std::vector<s4u::Link *> v_l_pmwmc1_pmnod22nic({l_pmwmc1_pmnod22nic});
    zone->add_route(pmwmc1, pmnod22nic->get_netpoint(), nullptr, nullptr, v_l_pmwmc1_pmnod22nic, true);
    std::vector<s4u::Link *> v_l_pmwmc1_pmnod19nic({l_pmwmc1_pmnod19nic});
    zone->add_route(pmwmc1, pmnod19nic->get_netpoint(), nullptr, nullptr, v_l_pmwmc1_pmnod19nic, true);
    std::vector<s4u::Link *> v_l_pmwmc1_pmnod25nic({l_pmwmc1_pmnod25nic});
    zone->add_route(pmwmc1, pmnod25nic->get_netpoint(), nullptr, nullptr, v_l_pmwmc1_pmnod25nic, true);
    std::vector<s4u::Link *> v_l_pmwmc1_pmnod33bullloc1nic({l_pmwmc1_pmnod33bullloc1nic});
    zone->add_route(pmwmc1, pmnod33bullloc1nic->get_netpoint(), nullptr, nullptr, v_l_pmwmc1_pmnod33bullloc1nic, true);
    std::vector<s4u::Link *> v_l_pmwmc1_pmnod30bullloc1nic({l_pmwmc1_pmnod30bullloc1nic});
    zone->add_route(pmwmc1, pmnod30bullloc1nic->get_netpoint(), nullptr, nullptr, v_l_pmwmc1_pmnod30bullloc1nic, true);
    std::vector<s4u::Link *> v_l_pmwmc1_pmnod33bulllocnic({l_pmwmc1_pmnod33bulllocnic});
    zone->add_route(pmwmc1, pmnod33bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc1_pmnod33bulllocnic, true);
    std::vector<s4u::Link *> v_l_pmwmc1_pmnod30bulllocnic({l_pmwmc1_pmnod30bulllocnic});
    zone->add_route(pmwmc1, pmnod30bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc1_pmnod30bulllocnic, true);
    std::vector<s4u::Link *> v_l_pmwmc1_pmwmc8({l_pmwmc1_pmwmc8_FAT, l_pmwmc1_pmwmc8});
    zone->add_route(pmwmc1, pmwmc8, nullptr, nullptr, v_l_pmwmc1_pmwmc8, true);
    std::vector<s4u::Link *> v_l_pmwmc1_pmwmc6({l_pmwmc1_pmwmc6_FAT, l_pmwmc1_pmwmc6});
    zone->add_route(pmwmc1, pmwmc6, nullptr, nullptr, v_l_pmwmc1_pmwmc6, true);
    std::vector<s4u::Link *> v_l_pmwmc1_pmnod24bullxnic({l_pmwmc1_pmnod24bullxnic});
    zone->add_route(pmwmc1, pmnod24bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc1_pmnod24bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc1_pmnod15nic({l_pmwmc1_pmnod15nic});
    zone->add_route(pmwmc1, pmnod15nic->get_netpoint(), nullptr, nullptr, v_l_pmwmc1_pmnod15nic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod11bullxnic({l_pmwmc2_pmnod11bullxnic});
    zone->add_route(pmwmc2, pmnod11bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod11bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod08bullxnic({l_pmwmc2_pmnod08bullxnic});
    zone->add_route(pmwmc2, pmnod08bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod08bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod10bullxnic({l_pmwmc2_pmnod10bullxnic});
    zone->add_route(pmwmc2, pmnod10bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod10bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod41bullxnic({l_pmwmc2_pmnod41bullxnic});
    zone->add_route(pmwmc2, pmnod41bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod41bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod38bullxnic({l_pmwmc2_pmnod38bullxnic});
    zone->add_route(pmwmc2, pmnod38bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod38bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod40bullxnic({l_pmwmc2_pmnod40bullxnic});
    zone->add_route(pmwmc2, pmnod40bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod40bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod37bullxnic({l_pmwmc2_pmnod37bullxnic});
    zone->add_route(pmwmc2, pmnod37bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod37bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod47bullxnic({l_pmwmc2_pmnod47bullxnic});
    zone->add_route(pmwmc2, pmnod47bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod47bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod44bullxnic({l_pmwmc2_pmnod44bullxnic});
    zone->add_route(pmwmc2, pmnod44bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod44bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod46bullxnic({l_pmwmc2_pmnod46bullxnic});
    zone->add_route(pmwmc2, pmnod46bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod46bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod43bullxnic({l_pmwmc2_pmnod43bullxnic});
    zone->add_route(pmwmc2, pmnod43bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod43bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod53bullxnic({l_pmwmc2_pmnod53bullxnic});
    zone->add_route(pmwmc2, pmnod53bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod53bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod50bullxnic({l_pmwmc2_pmnod50bullxnic});
    zone->add_route(pmwmc2, pmnod50bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod50bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod52bullxnic({l_pmwmc2_pmnod52bullxnic});
    zone->add_route(pmwmc2, pmnod52bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod52bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod49bullxnic({l_pmwmc2_pmnod49bullxnic});
    zone->add_route(pmwmc2, pmnod49bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod49bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmwmc8({l_pmwmc2_pmwmc8_FAT, l_pmwmc2_pmwmc8});
    zone->add_route(pmwmc2, pmwmc8, nullptr, nullptr, v_l_pmwmc2_pmwmc8, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmwmc6({l_pmwmc2_pmwmc6_FAT, l_pmwmc2_pmwmc6});
    zone->add_route(pmwmc2, pmwmc6, nullptr, nullptr, v_l_pmwmc2_pmwmc6, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod56bullxnic({l_pmwmc2_pmnod56bullxnic});
    zone->add_route(pmwmc2, pmnod56bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod56bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod55bullxnic({l_pmwmc2_pmnod55bullxnic});
    zone->add_route(pmwmc2, pmnod55bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod55bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod54bullxnic({l_pmwmc2_pmnod54bullxnic});
    zone->add_route(pmwmc2, pmnod54bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod54bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod51bullxnic({l_pmwmc2_pmnod51bullxnic});
    zone->add_route(pmwmc2, pmnod51bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod51bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod48bullxnic({l_pmwmc2_pmnod48bullxnic});
    zone->add_route(pmwmc2, pmnod48bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod48bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod45bullxnic({l_pmwmc2_pmnod45bullxnic});
    zone->add_route(pmwmc2, pmnod45bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod45bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod42bullxnic({l_pmwmc2_pmnod42bullxnic});
    zone->add_route(pmwmc2, pmnod42bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod42bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod39bullxnic({l_pmwmc2_pmnod39bullxnic});
    zone->add_route(pmwmc2, pmnod39bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod39bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod36bullxnic({l_pmwmc2_pmnod36bullxnic});
    zone->add_route(pmwmc2, pmnod36bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod36bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod09bulllocnic({l_pmwmc2_pmnod09bulllocnic});
    zone->add_route(pmwmc2, pmnod09bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod09bulllocnic, true);
    std::vector<s4u::Link *> v_l_pmwmc2_pmnod06bullxnic({l_pmwmc2_pmnod06bullxnic});
    zone->add_route(pmwmc2, pmnod06bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc2_pmnod06bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc3_pmnod05bullxnic({l_pmwmc3_pmnod05bullxnic});
    zone->add_route(pmwmc3, pmnod05bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc3_pmnod05bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc3_pmnod02bullxnic({l_pmwmc3_pmnod02bullxnic});
    zone->add_route(pmwmc3, pmnod02bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc3_pmnod02bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc3_pmnod04bullxnic({l_pmwmc3_pmnod04bullxnic});
    zone->add_route(pmwmc3, pmnod04bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc3_pmnod04bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc3_pmnod01bullxnic({l_pmwmc3_pmnod01bullxnic});
    zone->add_route(pmwmc3, pmnod01bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc3_pmnod01bullxnic, true);
    std::vector<s4u::Link *> v_l_pmwmc3_pmnod68bulllocnic({l_pmwmc3_pmnod68bulllocnic});
    zone->add_route(pmwmc3, pmnod68bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc3_pmnod68bulllocnic, true);
    std::vector<s4u::Link *> v_l_pmwmc3_pmnod70bulllocnic({l_pmwmc3_pmnod70bulllocnic});
    zone->add_route(pmwmc3, pmnod70bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc3_pmnod70bulllocnic, true);
    std::vector<s4u::Link *> v_l_pmwmc3_pmnod67bulllocnic({l_pmwmc3_pmnod67bulllocnic});
    zone->add_route(pmwmc3, pmnod67bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc3_pmnod67bulllocnic, true);
    std::vector<s4u::Link *> v_l_pmwmc3_pmwmc8({l_pmwmc3_pmwmc8_FAT, l_pmwmc3_pmwmc8});
    zone->add_route(pmwmc3, pmwmc8, nullptr, nullptr, v_l_pmwmc3_pmwmc8, true);
    std::vector<s4u::Link *> v_l_pmwmc3_pmwmc6({l_pmwmc3_pmwmc6_FAT, l_pmwmc3_pmwmc6});
    zone->add_route(pmwmc3, pmwmc6, nullptr, nullptr, v_l_pmwmc3_pmwmc6, true);
    std::vector<s4u::Link *> v_l_pmwmc3_pmnod69bulllocnic({l_pmwmc3_pmnod69bulllocnic});
    zone->add_route(pmwmc3, pmnod69bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc3_pmnod69bulllocnic, true);
    std::vector<s4u::Link *> v_l_pmwmc3_pmnod66bulllocnic({l_pmwmc3_pmnod66bulllocnic});
    zone->add_route(pmwmc3, pmnod66bulllocnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc3_pmnod66bulllocnic, true);
    std::vector<s4u::Link *> v_l_pmwmc3_mngt01nic({l_pmwmc3_mngt01nic});
    zone->add_route(pmwmc3, mngt01nic->get_netpoint(), nullptr, nullptr, v_l_pmwmc3_mngt01nic, true);
    std::vector<s4u::Link *> v_l_pmwmc3_pmnod00bullxnic({l_pmwmc3_pmnod00bullxnic});
    zone->add_route(pmwmc3, pmnod00bullxnic->get_netpoint(), nullptr, nullptr, v_l_pmwmc3_pmnod00bullxnic, true);

    zone->seal();

}
